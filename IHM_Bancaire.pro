QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/charts.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/parseJSON.cpp

HEADERS += \
    include/charts.hpp \
    include/mainwindow.h \
    include/mainwindow.hpp \
    include/parseJSON.hpp \
    include/ui_mainwindow.h \

FORMS += \
    forms/mainwindow.ui

RC_ICONS = Divers/icon.ico

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Divers/data_min.json \
    Divers/icon.ico
