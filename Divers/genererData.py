import datetime
import json
import random
import sys

listeAgences = ["Alpha", "Beta", "Charlie", "Delta"]

canaux = ["bancaire", "assurance", "boursier"]

produitsBancaires = ["pret", "compte", "chequier", "carte"]
typesPrets = ["habitation", "auto"]
typesComptes = ["courant", "epargne"]
typesAssurances = ["velo", "ordi", "animal"]

clients = []
employes = []
transactionsDict = {}
outputFile = 'data_min.json'


def parseNom(nom):
    nomSplit = nom.split(',')

    try:
        prenom = nomSplit[0]
        nomFamille = nomSplit[1].upper()
        return f'{prenom} {nomFamille}'

    except:
        print(f'Erreur split de: {nom}')
        sys.exit(1)


class Employe:
    def __init__(self, nomEmploye, agence):
        self.nomEmploye = parseNom(nomEmploye)
        self.agence = f'Agence {agence}'


class Produit:
    def __init__(self, canal, typeProduit, sousType):
        self.canal = canal
        self.typeProduit = typeProduit
        self.sousType = sousType
        self.prix = random.randint(100, 500)


class Transaction:
    def __init__(self, employe, produit, nomClient):
        self.agence = employe.agence
        self.nomEmploye = employe.nomEmploye
        self.canal = produit.canal
        self.typeProduit = produit.typeProduit
        self.sousType = produit.sousType
        self.prix = produit.prix
        self.nomClient = nomClient

    def toJSON(self):
        return {
            "agence": self.agence,
            "nomEmploye": self.nomEmploye,
            "canal": self.canal,
            "typeProduit": self.typeProduit,
            "sousType": self.sousType,
            "prix": self.prix,
            "nomClient": self.nomClient
        }


def nouveauProduit():
    canal = random.choice(canaux)
    sousType = ""

    if (canal == "bancaire"):
        typeProduit = random.choice(produitsBancaires)

        if (typeProduit == "chequiers") or (typeProduit == "cartes"):
            sousType = ""

        elif (typeProduit == "pret"):
            sousType = random.choice(typesPrets)

        elif (typeProduit == "compte"):
            sousType = random.choice(typesComptes)

    elif (canal == "assurance"):
        typeProduit = "assurance"
        sousType = random.choice(typesAssurances)

    elif (canal == "boursier"):
        typeProduit = "action"
        sousType = "action"

    return Produit(canal, typeProduit, sousType)


def genererTransactions(date):
    transactionsDict[date] = []

    # Pour toutes les agences
    for agence in listeAgences:
        # Filtrer les employes de cette agence:
        employesAgence = []
        for emp in employes:
            if (agence in emp.agence):
                employesAgence.append(emp)

        # 5 transactions par agence
        for _ in range(4):
            produit = nouveauProduit()
            client = random.choice(clients)
            employe = random.choice(employesAgence)
            t = Transaction(employe, produit, client)
            transactionsDict[date].append(t.toJSON())

    return


def genererDonnees():
    with open(outputFile, 'a+', encoding='utf-8') as f:
        unJour = datetime.timedelta(days=1)
        today = datetime.date.today()

        dateDepart = datetime.date(2021, 2, 1)
        dateFin = datetime.date(2021, 4, 18)

        d = dateDepart

        while (d != dateFin):
            if (d.weekday != 6):
                dateStr = d.strftime("%d/%m/%Y")
                genererTransactions(dateStr)
            d += unJour

        json.dump(transactionsDict, f)

    return


if __name__ == '__main__':
    with open("clients.txt", "r") as cli:
        for line in cli.read().splitlines():
            nomClient = parseNom(line.rstrip())
            clients.append(nomClient)

    nomEmployes = []
    with open("employes.txt", "r") as emp:
        for line in emp.read().splitlines():
            employe = line.rstrip()
            nomEmployes.append(employe)

    start = 0
    for nomAgence in listeAgences:
        end = start + 5
        for i in range(start, end):
            e = Employe(nomEmployes[i], nomAgence)
            employes.append(e)

        start = end

    with open(outputFile, 'w', encoding='utf-8') as f:
        f.truncate(0)

    genererDonnees()
