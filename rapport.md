Hakim ABDOUROIHAMANE - L3 S6 Informatique

## Projet IHM - Rapport

![Apercu](apercu.jpg)
[Lien vers la wireframe Ninjamock](https://ninjamock.com/s/3QTK6Hx) (ou se référer au fichier `wireframe.jpg`)

#### Jeu de données

Pour avoir un jeu de données suffisant, j'ai écrit un script Python qui crée un fichier `data_min.json` répétoriant toutes les ventes par date. Il suit la syntaxe suivante:

```
{
    Date: [
        {
            agence: "Nom agence",
            nomEmploye: "Nom employe",
            canal: "Nom canal",
            typeProduit: "Type produit",
            sousType: "sous-type",
            prix: prix (entier),
            nomClient: "Nom client"
        },
        ...
    ],
    ...
}
```

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

#### Fonctionnement de l'application

En haut de l'application, se trouvent tous les éléments pour pouvoir filtrer les données - de gauche à droite:

- deux widgets qui permettent de filtrer la période sur laquelle on veut afficher les données
  J'ai fait en sorte que l'utilisateur‧trice ne puisse pas entrer de date qui est en dehors de la période couverte par le jeu de données en paramétrant manuellement via Qt la date minimale/maximale que le widget accepte.

  J'ai mis des _tooltips_ pour signifier que le widget de gauche correspond à la date de début et celui de droite à la date de fin, mais en guise de sécurité dans le cas où la date de début serait postérieure à la date de fin, la fonction qui se charge de filtrer le JSON par date compare les deux dates qui lui sont passées en paramètre et les utilise "dans le bon ordre".

- une liste déroulante qui permet de filtrer le JSON par agence

- une liste déroulante qui permet de filtrer le JSON par employé‧e
  Quand une agence est séléctionnée, la liste déroulante est remplie avec le nom des employé‧e‧s de cette agence - si aucune agence n'est sélectionnée la liste reste vide.
  Là encore les deux listes déroulantes ont des _tooltips_ expliquant leur usage.

- deux boutons qui permettent d'appliquer ou de réinitialiser les filtres sélectionnés

La partie principale est constituée des trois zones représentant les données de chaque canal.
Chaque zone comprend un graphique et un groupe de trois boutons permettant de changer la vue des données:

- volume: histogramme représentant le nombre de ventes par type de produit
- euros: histogramme représentant le total en euros des ventes par type de produit
- pourcentage: diagramme circulaire repésentant la part de ventes par type de produit

Chaque bouton a un style particulier quand on clique dessus pour indiquer la vue active.

Dans les cas où les filtres renvoient un JSON vide (par exemple, si un‧e employé‧e n'a pas fait de ventes de produits bancaires sur la période donnée), la zone du graphique reste vide.
