/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "QtCharts"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QWidget *horizontalWidget;
    QHBoxLayout *horizontalLayout;
    QDateEdit *dateDebut;
    QDateEdit *dateFin;
    QComboBox *agencesComboBox;
    QComboBox *employesComboBox;
    QPushButton *appliquerFiltrer;
    QPushButton *resetFiltres;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *Bancaire;
    QVBoxLayout *verticalLayout_3;
    QChartView *chartsView_bancaire;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *volume_bancaire;
    QPushButton *euro_bancaire;
    QPushButton *pourcentage_bancaire;
    QGroupBox *Assurance;
    QVBoxLayout *verticalLayout_2;
    QChartView *chartsView_assurance;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *volume_assurance;
    QPushButton *euro_assurance;
    QPushButton *pourcentage_assurance;
    QGroupBox *Bourse;
    QVBoxLayout *verticalLayout_4;
    QChartView *chartsView_bourse;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *volume_bourse;
    QPushButton *euro_bourse;
    QPushButton *pourcentage_bourse;
    QMenuBar *menubar;
    QStatusBar *statusbar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1561, 759);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(4);
        MainWindow->setFont(font);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        sizePolicy.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy);
        centralwidget->setAutoFillBackground(false);
        centralwidget->setStyleSheet(QString::fromUtf8("centralwidget {\n"
"background: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(226, 226, 226, 255), stop:1 rgba(255, 255, 255, 255))\n"
"}"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setMaximumSize(QSize(11111111, 50));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Lato"));
        font1.setPointSize(26);
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        verticalLayout->addWidget(label, 0, Qt::AlignHCenter);

        horizontalWidget = new QWidget(centralwidget);
        horizontalWidget->setObjectName(QString::fromUtf8("horizontalWidget"));
        horizontalWidget->setMaximumSize(QSize(16777215, 75));
        horizontalLayout = new QHBoxLayout(horizontalWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinAndMaxSize);
        dateDebut = new QDateEdit(horizontalWidget);
        dateDebut->setObjectName(QString::fromUtf8("dateDebut"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Lato"));
        font2.setPointSize(8);
        dateDebut->setFont(font2);
        dateDebut->setToolTipDuration(3000);
        dateDebut->setMaximumDate(QDate(2021, 3, 17));
        dateDebut->setMinimumDate(QDate(2021, 2, 1));

        horizontalLayout->addWidget(dateDebut);

        dateFin = new QDateEdit(horizontalWidget);
        dateFin->setObjectName(QString::fromUtf8("dateFin"));
        dateFin->setFont(font2);
        dateFin->setToolTipDuration(3000);
        dateFin->setDateTime(QDateTime(QDate(2021, 4, 17), QTime(0, 0, 0)));
        dateFin->setMaximumDateTime(QDateTime(QDate(2021, 4, 17), QTime(23, 59, 59)));
        dateFin->setMaximumDate(QDate(2021, 4, 17));
        dateFin->setMinimumDate(QDate(2021, 2, 1));

        horizontalLayout->addWidget(dateFin);

        agencesComboBox = new QComboBox(horizontalWidget);
        agencesComboBox->setObjectName(QString::fromUtf8("agencesComboBox"));
        agencesComboBox->setFont(font2);
        agencesComboBox->setMouseTracking(true);
        agencesComboBox->setTabletTracking(true);
        agencesComboBox->setToolTipDuration(3000);
        agencesComboBox->setMinimumContentsLength(0);

        horizontalLayout->addWidget(agencesComboBox);

        employesComboBox = new QComboBox(horizontalWidget);
        employesComboBox->setObjectName(QString::fromUtf8("employesComboBox"));
        employesComboBox->setFont(font2);
        employesComboBox->setToolTipDuration(3000);

        horizontalLayout->addWidget(employesComboBox);

        appliquerFiltrer = new QPushButton(horizontalWidget);
        appliquerFiltrer->setObjectName(QString::fromUtf8("appliquerFiltrer"));
        appliquerFiltrer->setFont(font2);
        appliquerFiltrer->setToolTipDuration(-1);

        horizontalLayout->addWidget(appliquerFiltrer);

        resetFiltres = new QPushButton(horizontalWidget);
        resetFiltres->setObjectName(QString::fromUtf8("resetFiltres"));
        resetFiltres->setFont(font2);

        horizontalLayout->addWidget(resetFiltres);


        verticalLayout->addWidget(horizontalWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        Bancaire = new QGroupBox(centralwidget);
        Bancaire->setObjectName(QString::fromUtf8("Bancaire"));
        sizePolicy.setHeightForWidth(Bancaire->sizePolicy().hasHeightForWidth());
        Bancaire->setSizePolicy(sizePolicy);
        QFont font3;
        font3.setFamily(QString::fromUtf8("Lato"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setWeight(75);
        Bancaire->setFont(font3);
        Bancaire->setFlat(false);
        Bancaire->setCheckable(false);
        verticalLayout_3 = new QVBoxLayout(Bancaire);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        chartsView_bancaire = new QChartView(Bancaire);
        chartsView_bancaire->setObjectName(QString::fromUtf8("chartsView_bancaire"));

        verticalLayout_3->addWidget(chartsView_bancaire);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        volume_bancaire = new QPushButton(Bancaire);
        volume_bancaire->setObjectName(QString::fromUtf8("volume_bancaire"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Lato"));
        font4.setPointSize(12);
        font4.setBold(false);
        font4.setWeight(50);
        volume_bancaire->setFont(font4);

        horizontalLayout_5->addWidget(volume_bancaire);

        euro_bancaire = new QPushButton(Bancaire);
        euro_bancaire->setObjectName(QString::fromUtf8("euro_bancaire"));
        euro_bancaire->setFont(font4);

        horizontalLayout_5->addWidget(euro_bancaire);

        pourcentage_bancaire = new QPushButton(Bancaire);
        pourcentage_bancaire->setObjectName(QString::fromUtf8("pourcentage_bancaire"));
        pourcentage_bancaire->setFont(font4);

        horizontalLayout_5->addWidget(pourcentage_bancaire);


        verticalLayout_3->addLayout(horizontalLayout_5);


        horizontalLayout_2->addWidget(Bancaire);

        Assurance = new QGroupBox(centralwidget);
        Assurance->setObjectName(QString::fromUtf8("Assurance"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Assurance->sizePolicy().hasHeightForWidth());
        Assurance->setSizePolicy(sizePolicy2);
        Assurance->setFont(font3);
        verticalLayout_2 = new QVBoxLayout(Assurance);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        chartsView_assurance = new QChartView(Assurance);
        chartsView_assurance->setObjectName(QString::fromUtf8("chartsView_assurance"));

        verticalLayout_2->addWidget(chartsView_assurance);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        volume_assurance = new QPushButton(Assurance);
        volume_assurance->setObjectName(QString::fromUtf8("volume_assurance"));
        volume_assurance->setFont(font4);

        horizontalLayout_4->addWidget(volume_assurance);

        euro_assurance = new QPushButton(Assurance);
        euro_assurance->setObjectName(QString::fromUtf8("euro_assurance"));
        euro_assurance->setFont(font4);

        horizontalLayout_4->addWidget(euro_assurance);

        pourcentage_assurance = new QPushButton(Assurance);
        pourcentage_assurance->setObjectName(QString::fromUtf8("pourcentage_assurance"));
        pourcentage_assurance->setFont(font4);

        horizontalLayout_4->addWidget(pourcentage_assurance);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_2->addWidget(Assurance);

        Bourse = new QGroupBox(centralwidget);
        Bourse->setObjectName(QString::fromUtf8("Bourse"));
        sizePolicy.setHeightForWidth(Bourse->sizePolicy().hasHeightForWidth());
        Bourse->setSizePolicy(sizePolicy);
        Bourse->setFont(font3);
        verticalLayout_4 = new QVBoxLayout(Bourse);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        chartsView_bourse = new QChartView(Bourse);
        chartsView_bourse->setObjectName(QString::fromUtf8("chartsView_bourse"));

        verticalLayout_4->addWidget(chartsView_bourse);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        volume_bourse = new QPushButton(Bourse);
        volume_bourse->setObjectName(QString::fromUtf8("volume_bourse"));
        volume_bourse->setFont(font4);

        horizontalLayout_3->addWidget(volume_bourse);

        euro_bourse = new QPushButton(Bourse);
        euro_bourse->setObjectName(QString::fromUtf8("euro_bourse"));
        euro_bourse->setFont(font4);

        horizontalLayout_3->addWidget(euro_bourse);

        pourcentage_bourse = new QPushButton(Bourse);
        pourcentage_bourse->setObjectName(QString::fromUtf8("pourcentage_bourse"));
        pourcentage_bourse->setFont(font4);

        horizontalLayout_3->addWidget(pourcentage_bourse);


        verticalLayout_4->addLayout(horizontalLayout_3);


        horizontalLayout_2->addWidget(Bourse);


        verticalLayout->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1561, 11));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "IHM Bancaire", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "IHM Bancaire", nullptr));
#if QT_CONFIG(tooltip)
        dateDebut->setToolTip(QCoreApplication::translate("MainWindow", "D\303\251but de la p\303\251riode", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        dateFin->setToolTip(QCoreApplication::translate("MainWindow", "Fin de la p\303\251riode", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        agencesComboBox->setToolTip(QCoreApplication::translate("MainWindow", "Filtrer par agence", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        employesComboBox->setToolTip(QCoreApplication::translate("MainWindow", "Filtrer par employ\303\251\342\200\247e de l'agence s\303\251lectionn\303\251e", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        appliquerFiltrer->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        appliquerFiltrer->setText(QCoreApplication::translate("MainWindow", "Appliquer les filtres", nullptr));
        resetFiltres->setText(QCoreApplication::translate("MainWindow", "R\303\251initialiser les filtres", nullptr));
        Bancaire->setTitle(QCoreApplication::translate("MainWindow", "Produits bancaires", nullptr));
        volume_bancaire->setText(QCoreApplication::translate("MainWindow", "Volume", nullptr));
        euro_bancaire->setText(QCoreApplication::translate("MainWindow", "Euro", nullptr));
        pourcentage_bancaire->setText(QCoreApplication::translate("MainWindow", "Pourcentage", nullptr));
        Assurance->setTitle(QCoreApplication::translate("MainWindow", "Produits d'assurance", nullptr));
        volume_assurance->setText(QCoreApplication::translate("MainWindow", "Volume ", nullptr));
        euro_assurance->setText(QCoreApplication::translate("MainWindow", "Euro", nullptr));
        pourcentage_assurance->setText(QCoreApplication::translate("MainWindow", "Pourcentage", nullptr));
        Bourse->setTitle(QCoreApplication::translate("MainWindow", "Produits boursiers", nullptr));
        volume_bourse->setText(QCoreApplication::translate("MainWindow", "Volume", nullptr));
        euro_bourse->setText(QCoreApplication::translate("MainWindow", "Euro", nullptr));
        pourcentage_bourse->setText(QCoreApplication::translate("MainWindow", "Pourcentage", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
