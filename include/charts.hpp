#ifndef CHARTS_HPP
#define CHARTS_HPP
#include <QJsonObject>
#include <QString>
#include <QtCharts>

QString getTitre(QString canal, QString vue);
QChart *creerChart(QString canal, QString vue, QJsonObject json);
QChart *creerChartPourcentage(QString canal, QJsonObject jsonData);
#endif // CHARTS_HPP
