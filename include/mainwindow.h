#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  QJsonObject jsonInitial;
  QJsonObject jsonFiltre;
  QString vueBancaire;
  QString vueAssurance;
  QString vueBourse;
  QDate dateDebut;
  QDate dateFin;
  QString agence;
  QString employe;

 private:
  void reactualiserJSON();
  void reactualiserComboBoxAgence();
  void reactualiserComboBoxEmploye();
  void appliquerFiltre();
  void activerBouton(QString canal);

 private slots:
  void on_volume_bancaire_clicked();

  void on_euro_bancaire_clicked();

  void on_pourcentage_bancaire_clicked();

  void on_volume_assurance_clicked();

  void on_euro_assurance_clicked();

  void on_pourcentage_assurance_clicked();

  void on_volume_bourse_clicked();

  void on_euro_bourse_clicked();

  void on_pourcentage_bourse_clicked();

  void on_agencesComboBox_textActivated(const QString &arg1);

  void on_appliquerFiltrer_clicked();

  void on_resetFiltres_clicked();

 private:
  Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
