#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QChartView>
#include <QtCharts>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class MainWindow : public QWidget {
 public:
  MyWindow();
  void setupUi(QMainWindow *MainWindow);
  void retranslateUi(QMainWindow *MainWindow);
  void setVue(QString canal, QString vue);

  // Pour stocker les différentes vues des 3 graphiques (volume, euro,
  // pourcentage);

 public slots:
  void changerLargeur(int largeur);

 private:
  QWidget *centralwidget;
  QWidget *horizontalLayoutWidget;
  QHBoxLayout *horizontalLayout;
  QDateEdit *dateDebut;
  QDateEdit *dateFin;
  QComboBox *agencesComboBox;
  QComboBox *employesComboBox;
  QWidget *verticalLayoutWidget_3;
  QHBoxLayout *horizontalLayout_2;
  QGroupBox *Bancaire;
  QChartView *chartsView_bancaire;
  QWidget *verticalLayoutWidget_4;
  QHBoxLayout *horizontalLayout_5;
  QPushButton *volume_bancaire;
  QPushButton *euro_bancaire;
  QPushButton *pourcentage_bancaire;
  QGroupBox *Assurance;
  QChartView *chartsView_assurance;
  QWidget *verticalLayoutWidget_2;
  QHBoxLayout *horizontalLayout_4;
  QPushButton *volume_assurance;
  QPushButton *euro_assurance;
  QPushButton *pourcentage_assurance;
  QGroupBox *Bourse;
  QChartView *chartsView_bourse;
  QWidget *verticalLayoutWidget;
  QHBoxLayout *horizontalLayout_3;
  QPushButton *volume_bourse;
  QPushButton *euro_bourse;
  QPushButton *pourcentage_bourse;
  QLabel *label;
  QMenuBar *menubar;
  QStatusBar *statusbar;

  QString vueBancaire;
  QString vueAssurance;
  QString vueBoursiere;
}

#endif  // MAINWINDOW_HPP
