#ifndef PARSEJSON_HPP
#define PARSEJSON_HPP

#include <QDate>
#include <QJsonObject>
#include <QList>
#include <QString>

QJsonObject readJSON();
QJsonObject filterDate(QDate debut, QDate fin, QJsonObject json);
QJsonObject filtrer(QString sousCle, QString valeur, QJsonObject json);
QJsonObject filtrerCanal(QString nomCanal, QJsonObject json);
QJsonObject filtrerSousType(QString nomSousType, QJsonObject json);
QList<QString> getDifferentValues(QString findKey, QJsonObject json);
QList<QString> listeAgences(QJsonObject json);
QList<QString> listeEmployes(QString agence, QJsonObject json);
QJsonObject calculerVolumes(QJsonObject json);
QJsonObject calculerVolumesBancaires(QJsonObject json);
QJsonObject calculerTotaux(QJsonObject json, QString parametreFiltre);
QJsonObject calculerPourcentages(QJsonObject totaux);
QJsonObject getExtremum(QJsonObject json);
QJsonObject filtrerGlobal(QDate debut, QDate fin, QString nomAgence,
                          QString nomEmploye, QJsonObject json);
QString getParametreTri(QString canal);

#endif // PARSEJSON_HPP
