#include "include/parseJSON.hpp"

#include <QDate>
#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QTextStream>
#include <QVector>
#include <fstream>
#include <iostream>

QJsonObject readJSON() {
  QString filename("Divers/data_min.json");
  QFile file(filename);
  bool fileOpened = file.open(QIODevice::ReadOnly);

  if (!fileOpened) {
    qDebug("Failed to open data_min.json");
    exit(1);
  }

  QByteArray bytes = file.readAll();
  file.close();

  QJsonParseError jsonError;
  QJsonDocument document = QJsonDocument::fromJson(bytes, &jsonError);
  if (jsonError.error != QJsonParseError::NoError) {
    qDebug() << "fromJson failed: " << jsonError.errorString() << endl;
    exit(1);
  }

  QJsonObject jsonObj = document.object();
  return jsonObj;
}

QJsonObject filterDate(QDate date_1, QDate date_2, QJsonObject json) {
  QJsonObject filtered = {};
  QStringList keys = json.keys();
  QDate debut, fin;

  // Gérer les cas où la date de début est postérieure à la date de fin
  debut = (date_1 < date_2) ? date_1 : date_2;
  fin = (debut == date_1) ? date_2 : date_1;

  for (int i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QDate dateKey = QDate::fromString(date, "dd/MM/yyyy");

    if ((debut <= dateKey) && (dateKey <= fin)) {
      filtered.insert(date, json.value(date));
    }
  }

  return filtered;
}

QJsonObject filtrer(QString sousCle, QString valeur, QJsonObject json) {
  QJsonObject filtered = {};

  QStringList keys = json.keys();
  int i, j;

  for (i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QJsonArray array;
    QJsonArray subArray = json[date].toArray();

    for (j = 0; j < subArray.size(); j++) {
      QJsonObject obj = subArray[j].toObject();
      if (obj[sousCle].toString() == valeur) array.append(obj);
    }

    filtered.insert(date, array);
  }
  return filtered;
}

QJsonObject filtrerCanal(QString nomCanal, QJsonObject json) {
  return filtrer("canal", nomCanal, json);
}

// filtrer par sous-type
QJsonObject filtrerSousType(QString nomSousType, QJsonObject json) {
  return filtrer("sousType", nomSousType, json);
}

// Get les différents sous-types
QList<QString> getDifferentValues(QString findKey, QJsonObject json) {
  QList<QString> list;
  QStringList keys = json.keys();
  int i, j;

  for (i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QJsonArray subArray = json.value(date).toArray();

    for (j = 0; j < subArray.size(); j++) {
      QJsonObject obj = subArray[j].toObject();
      QString valueStr = obj.value(findKey).toString();
      if ((valueStr.length() > 0) && (!list.contains(valueStr))) {
        list.append(valueStr);
      }
    }
  }

  return list;
}

QList<QString> listeAgences(QJsonObject json) {
  return getDifferentValues("agence", json);
}

QList<QString> listeEmployes(QString agence, QJsonObject json) {
  QList<QString> list;
  if (!agence.toLower().startsWith("aucun")) {
    QJsonObject jsonAgence = filtrer("agence", agence, json);
    QStringList keys = jsonAgence.keys();
    int i, j;

    for (i = 0; i < keys.length(); i++) {
      QString date = keys[i];
      QJsonArray subArray = jsonAgence.value(date).toArray();

      for (j = 0; j < subArray.size(); j++) {
        QJsonObject obj = subArray[j].toObject();
        QString nomEmploye = obj.value("nomEmploye").toString();
        if (!list.contains(nomEmploye)) {
          list.append(nomEmploye);
        }
      }
    }
  }

  return list;
}

// Calculer le volume, pourcentage et total en euros
QJsonObject calculerVolumes(QJsonObject json) {
  QJsonObject totaux = {};
  QStringList keys = json.keys();
  int i, j;

  for (i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QJsonArray subArray = json.value(date).toArray();

    for (j = 0; j < subArray.size(); j++) {
      QJsonObject obj = subArray[j].toObject();
      QString sousType = obj.value("sousType").toString();

      if (!totaux.contains(sousType)) {
        totaux.insert(sousType, 1);
      }

      else {
        int valeurActuelle = totaux.value(sousType).toInt();
        totaux.remove(sousType);
        totaux.insert(sousType, valeurActuelle + 1);
      }
    }
  }

  return totaux;
}

QJsonObject calculerVolumesBancaires(QJsonObject json) {
  QJsonObject totaux = {};
  QStringList keys = json.keys();
  int i, j;

  for (i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QJsonArray subArray = json.value(date).toArray();

    for (j = 0; j < subArray.size(); j++) {
      QJsonObject obj = subArray[j].toObject();
      QString sousType = obj.value("typeProduit").toString();

      if (!totaux.contains(sousType)) {
        totaux.insert(sousType, 1);
      }

      else {
        int valeurActuelle = totaux.value(sousType).toInt();
        totaux.remove(sousType);
        totaux.insert(sousType, valeurActuelle + 1);
      }
    }
  }

  return totaux;
}

QJsonObject calculerTotaux(QJsonObject json, QString parametreFiltre) {
  QJsonObject totaux = {};
  QStringList keys = json.keys();
  int i, j;

  for (i = 0; i < keys.length(); i++) {
    QString date = keys[i];
    QJsonArray subArray = json.value(date).toArray();

    for (j = 0; j < subArray.size(); j++) {
      QJsonObject obj = subArray[j].toObject();
      QString sousType = obj.value(parametreFiltre).toString();
      int prix = obj.value("prix").toInt();

      if (!totaux.contains(sousType)) {
        totaux.insert(sousType, prix);
      }

      else {
        int valeurActuelle = totaux.value(sousType).toInt();
        totaux.remove(sousType);
        totaux.insert(sousType, valeurActuelle + prix);
      }
    }
  }

  return totaux;
}

QJsonObject calculerPourcentages(QJsonObject totaux) {
  QJsonObject pourcentagesJSON = {};
  double pourcentage;
  int totalGlobal = 0;
  int i;

  QStringList keys = totaux.keys();

  for (i = 0; i < keys.length(); i++) {
    QString sousType = keys[i];
    totalGlobal += totaux.value(sousType).toInt();
  }

  for (i = 0; i < keys.length(); i++) {
    QString sousType = keys[i];
    pourcentage = totaux.value(sousType).toInt() * 100. / totalGlobal;
    pourcentagesJSON.insert(sousType, pourcentage);
  }

  return pourcentagesJSON;
}

QJsonObject getExtremum(QJsonObject json) {
  double min = INT_MAX - 0.01;
  double max = INT_MIN + 0.01;
  double value;

  foreach (const QString key, json.keys()) {
    value = json.value(key).toDouble();

    if (value > max) {
      max = value;
    }

    if (value < min) {
      min = value;
    }
  }

  QJsonObject extremum;
  extremum.insert("min", min);
  extremum.insert("max", max);
  return extremum;
}

QJsonObject filtrerGlobal(QDate debut, QDate fin, QString nomAgence,
                          QString nomEmploye, QJsonObject json) {
  QJsonObject temp = json;
  temp = filterDate(debut, fin, temp);

  if (!nomAgence.toLower().startsWith("aucun")) {
    temp = filtrer("agence", nomAgence, temp);
  }

  if (!nomEmploye.toLower().startsWith("aucun")) {
    temp = filtrer("nomEmploye", nomEmploye, temp);
  }

  return temp;
}

QString getParametreTri(QString canal) {
  QString p;

  if (canal == "bancaire") {
    p = "typeProduit";
  }

  else if (canal == "assurance") {
    p = "sousType";
  }

  else if (canal == "boursier") {
    p = "typeProduit";
  }

  else {
    qDebug() << "Erreur canal inconnu - " << canal << endl;
    exit(1);
  }

  return p;
}
