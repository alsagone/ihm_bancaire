#include "include/charts.hpp"

#include <QBarSeries>
#include <QColor>
#include <QJsonObject>
#include <QString>
#include <QtCharts>
#include <QtDebug>
#include <cmath>

#include "include/parseJSON.hpp"

QString getTitre(QString canal, QString vue) {
  QString titre;

  if (canal == "bancaire") {
    titre = "Produits bancaires";
  }

  else if (canal == "assurance") {
    titre = "Produits d'assurance";
  }

  else if (canal == "boursier") {
    titre = "Produits boursiers";
  }

  else {
    qDebug() << "Erreur - canal inconnu - " << canal;
  }

  if (vue == "volume") {
    titre += " - Vue par volume";
  }

  else if (vue == "euros") {
    titre += " - Vue par total en euros";
  }

  else if (vue == "pourcentage") {
    titre += " - Vue par pourcentage";
  }

  else {
    qDebug() << "Erreur - vue inconnue - " << vue;
  }

  return titre;
}

QChart *creerChart(QString canal, QString vue, QJsonObject jsonData) {
  QChart *chart = new QChart();
  QString parametreTri = getParametreTri(canal);

  QJsonObject jsonFiltre = filtrerCanal(canal, jsonData);

  QStringList categories = getDifferentValues(parametreTri, jsonFiltre);

  QBarSeries *series = new QBarSeries();
  QBarSet *set = new QBarSet("monSet");
  QJsonObject extremum;
  double valeur;

  if (vue == "pourcentage") {
    return creerChartPourcentage(canal, jsonData);
  }

  else if (vue == "volume") {
    QJsonObject volumes;

    if (canal == "bancaire") {
      volumes = calculerVolumesBancaires(jsonFiltre);
    }

    else {
      volumes = calculerVolumes(jsonFiltre);
    }

    extremum = getExtremum(volumes);

    foreach (QString sousType, categories) {
      valeur = volumes.value(sousType).toInt();
      set->append(valeur);
    }
  }

  else if (vue == "euros") {
    QJsonObject totaux = calculerTotaux(jsonFiltre, parametreTri);
    extremum = getExtremum(totaux);

    foreach (QString sousType, categories) {
      valeur = totaux.value(sousType).toDouble();
      set->append(valeur);
    }

  }

  else {
    qDebug() << "Erreur: vue inconnue - " << vue << endl;
    exit(1);
  }

  series->append(set);
  series->setLabelsVisible(true);
  series->setLabelsPosition(QAbstractBarSeries::LabelsInsideEnd);
  chart->addSeries(series);

  QString titre = getTitre(canal, vue);
  chart->setTitle(titre);
  chart->setAnimationOptions(QChart::SeriesAnimations);

  QBarCategoryAxis *axisX = new QBarCategoryAxis();
  axisX->append(categories);
  chart->addAxis(axisX, Qt::AlignBottom);
  series->attachAxis(axisX);

  QValueAxis *axisY = new QValueAxis();

  // Min value = minimum observé - 10%
  qreal minValue = floor(0.9 * extremum.value("min").toDouble());

  // Max value = maximum observé + 1%
  qreal maxValue = ceil(1.01 * extremum.value("max").toDouble());

  axisY->setRange(minValue, maxValue);
  chart->addAxis(axisY, Qt::AlignLeft);
  series->attachAxis(axisY);
  chart->legend()->setVisible(false);

  return chart;
}

QChart *creerChartPourcentage(QString canal, QJsonObject jsonData) {
  QString titre = getTitre(canal, "pourcentage");
  QChart *chart = new QChart();
  chart->setTitle(titre);

  QString parametreTri = getParametreTri(canal);
  QJsonObject jsonFiltre = filtrerCanal(canal, jsonData);
  QStringList categories = getDifferentValues(parametreTri, jsonFiltre);
  QJsonObject totaux = calculerTotaux(jsonFiltre, parametreTri);
  QJsonObject pourcentages = calculerPourcentages(totaux);

  QPieSeries *series = new QPieSeries();
  double valeur;

  foreach (QString sousType, categories) {
    valeur = pourcentages.value(sousType).toDouble();
    series->append(sousType, valeur);
  }

  series->setLabelsVisible();
  series->setLabelsPosition(QPieSlice::LabelInsideHorizontal);
  chart->setAnimationOptions(QChart::AllAnimations);

  QColor white = QColor::fromRgb(255, 255, 255, 255);
  QString label, pourcentageStr;

  for (auto slice : series->slices()) {
    slice->setLabelColor(white);
    pourcentageStr = QString("%1%").arg(100 * slice->percentage(), 0, 'f', 1);

    label = slice->label() + " " + pourcentageStr;
    slice->setLabel(label);
  }

  chart->legend()->setVisible(false);
  chart->addSeries(series);

  return chart;
}
