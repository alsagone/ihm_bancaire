#include "include/mainwindow.h"

#include "include/charts.hpp"
#include "include/parseJSON.hpp"
#include "include/ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::MainWindow)
{
  this->jsonInitial = readJSON();
  this->jsonFiltre = this->jsonInitial;

  this->vueBancaire = "volume";
  this->vueAssurance = "pourcentage";
  this->vueBourse = "euros";

  ui->setupUi(this);
  this->dateDebut = ui->dateDebut->date();
  this->dateFin = ui->dateFin->date();
  this->agence = "Aucune";
  this->employe = "Aucun";

  this->on_volume_bancaire_clicked();
  this->on_pourcentage_assurance_clicked();
  this->on_euro_bourse_clicked();
  this->reactualiserComboBoxAgence();
  this->reactualiserJSON();
  this->reactualiserComboBoxEmploye();
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::reactualiserJSON() {
  this->jsonFiltre = filtrerGlobal(this->dateDebut, this->dateFin, this->agence,
                                   this->employe, this->jsonInitial);
}

void MainWindow::reactualiserComboBoxAgence() {
  this->jsonFiltre =
      filterDate(this->dateDebut, this->dateFin, this->jsonInitial);

  // vider la combo box
  ui->agencesComboBox->clear();

  // remplir la combo box avec toutes les agences disponibles dans le JSON
  foreach (QString agence, listeAgences(this->jsonFiltre)) {
    ui->agencesComboBox->addItem(agence);
  }

  ui->agencesComboBox->insertItem(0, "Aucune");
  ui->agencesComboBox->setCurrentIndex(0);
  return;
}

void MainWindow::reactualiserComboBoxEmploye() {
  // La même chose pour les employé‧e‧s
  ui->employesComboBox->clear();
  this->jsonFiltre =
      filterDate(this->dateDebut, this->dateFin, this->jsonInitial);
  this->jsonFiltre = filtrer("agence", this->agence, this->jsonFiltre);

  foreach (QString employe, listeEmployes(this->agence, this->jsonFiltre)) {
    ui->employesComboBox->addItem(employe);
  }

  ui->employesComboBox->insertItem(0, "Aucun");
  ui->employesComboBox->setCurrentIndex(0);
  return;
}

void MainWindow::appliquerFiltre() {
  // Filtrer le JSON selon les dates, l'agence et l'employé sélectionné
  reactualiserJSON();

  // Créer les graphiques et les afficher
  QChart *chartBancaire =
      creerChart("bancaire", this->vueBancaire, this->jsonFiltre);
  QChart *chartAssurance =
      creerChart("assurance", this->vueAssurance, this->jsonFiltre);
  QChart *chartBourse =
      creerChart("boursier", this->vueBourse, this->jsonFiltre);
  ui->chartsView_bancaire->setChart(chartBancaire);
  ui->chartsView_assurance->setChart(chartAssurance);
  ui->chartsView_bourse->setChart(chartBourse);
}

// Gère la mise en évidence des boutons en bleu quand on clique dessus
void MainWindow::activerBouton(QString canal) {
  QString stylesheet =
      "background-color: #209fdf; color: #fff; font-weight: bold;";

  if (canal == "bancaire") {
    if (this->vueBancaire == "volume") {
      ui->volume_bancaire->setStyleSheet(stylesheet);
      ui->euro_bancaire->setStyleSheet("");
      ui->pourcentage_bancaire->setStyleSheet("");
    }

    else if (this->vueBancaire == "euros") {
      ui->volume_bancaire->setStyleSheet("");
      ui->euro_bancaire->setStyleSheet(stylesheet);
      ui->pourcentage_bancaire->setStyleSheet("");
    }

    else if (this->vueBancaire == "pourcentage") {
      ui->volume_bancaire->setStyleSheet("");
      ui->euro_bancaire->setStyleSheet("");
      ui->pourcentage_bancaire->setStyleSheet(stylesheet);
    }

    else {
      qDebug() << "Vue bancaire inconnue: " << this->vueBancaire;
    }
  }

  else if (canal == "assurance") {
    if (this->vueAssurance == "volume") {
      ui->volume_assurance->setStyleSheet(stylesheet);
      ui->euro_assurance->setStyleSheet("");
      ui->pourcentage_assurance->setStyleSheet("");
    }

    else if (this->vueAssurance == "euros") {
      ui->volume_assurance->setStyleSheet("");
      ui->euro_assurance->setStyleSheet(stylesheet);
      ui->pourcentage_assurance->setStyleSheet("");
    }

    else if (this->vueAssurance == "pourcentage") {
      ui->volume_assurance->setStyleSheet("");
      ui->euro_assurance->setStyleSheet("");
      ui->pourcentage_assurance->setStyleSheet(stylesheet);
    }

    else {
      qDebug() << "Vue assurance inconnue: " << this->vueAssurance;
    }
  }

  else if (canal == "boursier") {
    if (this->vueBourse == "volume") {
      ui->volume_bourse->setStyleSheet(stylesheet);
      ui->euro_bourse->setStyleSheet("");
      ui->pourcentage_bourse->setStyleSheet("");
    }

    else if (this->vueBourse == "euros") {
      ui->volume_bourse->setStyleSheet("");
      ui->euro_bourse->setStyleSheet(stylesheet);
      ui->pourcentage_bourse->setStyleSheet("");
    }

    else if (this->vueBourse == "pourcentage") {
      ui->volume_bourse->setStyleSheet("");
      ui->euro_bourse->setStyleSheet("");
      ui->pourcentage_bourse->setStyleSheet(stylesheet);
    }

    else {
      qDebug() << "Vue boursière inconnue: " << this->vueBourse;
    }
  }

  else {
    qDebug() << "Canal inconnu: " << canal;
  }
}

// Afficher le graphique bancaire_volume
void MainWindow::on_volume_bancaire_clicked() {
  this->vueBancaire = "volume";
  activerBouton("bancaire");
  reactualiserJSON();
  QChart *chart = creerChart("bancaire", this->vueBancaire, this->jsonFiltre);
  ui->chartsView_bancaire->setChart(chart);
}

// Afficher le graphique bancaire_euros
void MainWindow::on_euro_bancaire_clicked() {
  this->vueBancaire = "euros";
  activerBouton("bancaire");
  reactualiserJSON();
  QChart *chart = creerChart("bancaire", this->vueBancaire, this->jsonFiltre);
  ui->chartsView_bancaire->setChart(chart);
}

// Afficher le graphique bancaire_pourcentage
void MainWindow::on_pourcentage_bancaire_clicked() {
  this->vueBancaire = "pourcentage";
  activerBouton("bancaire");
  reactualiserJSON();
  QChart *chart = creerChart("bancaire", this->vueBancaire, this->jsonFiltre);
  ui->chartsView_bancaire->setChart(chart);
}

// Afficher le graphique assurance_volume
void MainWindow::on_volume_assurance_clicked() {
  this->vueAssurance = "volume";
  activerBouton("assurance");
  reactualiserJSON();
  QChart *chart = creerChart("assurance", this->vueAssurance, this->jsonFiltre);
  ui->chartsView_assurance->setChart(chart);
}

// Afficher le graphique assurance_euros
void MainWindow::on_euro_assurance_clicked() {
  this->vueAssurance = "euros";
  activerBouton("assurance");
  reactualiserJSON();
  QChart *chart = creerChart("assurance", this->vueAssurance, this->jsonFiltre);
  ui->chartsView_assurance->setChart(chart);
}

// Afficher le graphique assurance_pourcentage
void MainWindow::on_pourcentage_assurance_clicked() {
  this->vueAssurance = "pourcentage";
  activerBouton("assurance");
  reactualiserJSON();
  QChart *chart = creerChart("assurance", this->vueAssurance, this->jsonFiltre);
  ui->chartsView_assurance->setChart(chart);
}

// Afficher le graphique bourse_volume
void MainWindow::on_volume_bourse_clicked() {
  this->vueBourse = "volume";
  activerBouton("boursier");
  reactualiserJSON();
  QChart *chart = creerChart("boursier", this->vueBourse, this->jsonFiltre);
  ui->chartsView_bourse->setChart(chart);
}

// Afficher le graphique bourse_euros
void MainWindow::on_euro_bourse_clicked() {
  this->vueBourse = "euros";
  activerBouton("boursier");
  reactualiserJSON();
  QChart *chart = creerChart("boursier", this->vueBourse, this->jsonFiltre);
  ui->chartsView_bourse->setChart(chart);
}

// Afficher le graphique bourse_pourcentage
void MainWindow::on_pourcentage_bourse_clicked() {
  this->vueBourse = "pourcentage";
  activerBouton("boursier");
  reactualiserJSON();
  QChart *chart = creerChart("boursier", this->vueBourse, this->jsonFiltre);
  ui->chartsView_bourse->setChart(chart);
}

// Si on sélectionne une agence, on réactualise la liste des employés
void MainWindow::on_agencesComboBox_textActivated(const QString &arg1) {
  this->agence = arg1;
  this->jsonFiltre = filtrer("agence", this->agence, this->jsonFiltre);
  reactualiserComboBoxEmploye();
}

// Filtrer le json et afficher les graphiques résultant
void MainWindow::on_appliquerFiltrer_clicked() {
  this->dateDebut = ui->dateDebut->date();
  this->dateFin = ui->dateFin->date();
  this->agence = ui->agencesComboBox->currentText();
  this->employe = ui->employesComboBox->currentText();
  appliquerFiltre();
}

// Reset les combobox et réinitialser le json à son état initial
void MainWindow::on_resetFiltres_clicked() {
  this->agence = "Aucune";
  this->employe = "Aucun";
  this->jsonFiltre = this->jsonInitial;
  appliquerFiltre();
  reactualiserComboBoxAgence();
  reactualiserComboBoxEmploye();
}
